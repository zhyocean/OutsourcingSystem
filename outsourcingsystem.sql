/*
Navicat MySQL Data Transfer

Source Server         : 119.23.202.55
Source Server Version : 50641
Source Host           : 119.23.202.55:3306
Source Database       : outsourcingsystem

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2019-04-22 11:05:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dynamicinformation
-- ----------------------------
DROP TABLE IF EXISTS `dynamicinformation`;
CREATE TABLE `dynamicinformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `uploader` varchar(255) NOT NULL,
  `uploadTime` varchar(255) NOT NULL,
  `uploadInstructions` varchar(255) NOT NULL,
  `progress` int(11) NOT NULL,
  `filePath` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dynamicinformation
-- ----------------------------

-- ----------------------------
-- Table structure for orgztree
-- ----------------------------
DROP TABLE IF EXISTS `orgztree`;
CREATE TABLE `orgztree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orgztree
-- ----------------------------

-- ----------------------------
-- Table structure for outsourcinginfo
-- ----------------------------
DROP TABLE IF EXISTS `outsourcinginfo`;
CREATE TABLE `outsourcinginfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL,
  `category` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `publishTime` varchar(255) NOT NULL,
  `requirement` varchar(255) NOT NULL,
  `registrationDeadline` varchar(255) NOT NULL,
  `projectDeadline` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `prospectus` varchar(255) NOT NULL,
  `progress` int(11) NOT NULL,
  `contract` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of outsourcinginfo
-- ----------------------------
INSERT INTO `outsourcinginfo` VALUES ('1', '报名中', '求购设备指纹算法', '3', '算法', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '5000', '2018/11/19', '0', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('2', '报名中', '\r\n微信小程序页面载入速度自动测试', '3', '微信/小程序开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '15000', '2018/11/19', '0', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('3', '实施中', '\r\n客户端测试', '3', '网站开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '900', '2018/11/19', '20', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('4', '已完成', '驱动控制台式机和笔记本电脑开机.重启功能', '3', '网站开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '10000', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('5', '实施中', '\r\n光端机复合千兆以太网开发', '3', '其他', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '15000', '2018/11/19', '50', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('6', '实施中', '合泰单片机BS83B08A增加遥控功能', '3', '其他', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '23000', '2018/11/19', '30', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('7', '已完成', '并行算法文章代写', '3', '嵌入式系统', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '2000', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('8', '实施中', '支付端口软件系统开发', '3', '移动开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '1000', '2018/11/19', '15', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('9', '已完成', '寻找指纹兼容算法', '3', '算法', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '500', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('10', '已完成', '\r\n微信机器人', '3', '微信/小程序开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '50000', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('11', '实施中', '点餐小程序', '3', '移动开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '3000', '2018/11/19', '80', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('12', '已完成', '图像识别分割算法', '3', '算法', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '700', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('13', '报名中', '图像标定软件的开发', '3', '嵌入式系统', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '50000', '2018/11/19', '0', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('14', '实施中', '桌面直播软件', '3', '嵌入式系统', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '6000', '2018/11/19', '55', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('15', '已完成', '人员调动系统（放在企业微信中）', '3', '嵌入式系统', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '12000', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('16', '报名中', '生产企业应用小程序', '3', '微信/小程序开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '2200', '2018/11/19', '0', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('17', '实施中', '定制设计PCB', '3', '嵌入式系统', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '3000', '2018/11/19', '10', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('18', '已完成', 'windows usb 蓝牙（CSR4.0）开发', '3', '人工智能', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '3200', '2018/11/19', '100', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('19', '报名中', '网站开发', '3', '网站开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '3500', '2018/11/19', '0', '2018/11/19');
INSERT INTO `outsourcinginfo` VALUES ('20', '实施中', '在线试题库', '3', '网站开发', ' 一、包含指纹提取，对比，指纹识别\r\n1、指纹提取包括浏览器PC、移动端采集sdk；\r\n2、主动式或被动式采集或混合式方案；\r\n3、指纹对比以及存储方案；\r\n4、准确率测试； ', '19940790216', '2018/11/19', '指纹提取包括浏览器PC、移动端采集sdk;主动式或被动式采集或混合式方案；指纹对比以及存储方案；有相关指纹算法经验者优先，价格可谈。', '2018/11/19', '2018-11-19', '5000', '2018/11/19', '5', '2018/11/19');

-- ----------------------------
-- Table structure for outsourcinguserinfo
-- ----------------------------
DROP TABLE IF EXISTS `outsourcinguserinfo`;
CREATE TABLE `outsourcinguserinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `managerPhone` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `project` varchar(255) NOT NULL,
  `promise` varchar(255) NOT NULL,
  `contract` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of outsourcinguserinfo
-- ----------------------------

-- ----------------------------
-- Table structure for resourceinfo
-- ----------------------------
DROP TABLE IF EXISTS `resourceinfo`;
CREATE TABLE `resourceinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promulgator` varchar(255) NOT NULL,
  `taskName` varchar(255) NOT NULL,
  `resourcePath` varchar(255) NOT NULL,
  `releaseTime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resourceinfo
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'ROLE_USER');
INSERT INTO `role` VALUES ('2', 'ROLE_ADMIN');
INSERT INTO `role` VALUES ('3', 'ROLE_SUPERADMIN');

-- ----------------------------
-- Table structure for sign_records
-- ----------------------------
DROP TABLE IF EXISTS `sign_records`;
CREATE TABLE `sign_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `come_time` bigint(20) NOT NULL,
  `leave_time` bigint(20) DEFAULT NULL,
  `total_time` bigint(20) DEFAULT NULL,
  `str_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sign_records
-- ----------------------------
INSERT INTO `sign_records` VALUES ('1', '19940790216', '1542879568', '1542879586', '18', '18秒');
INSERT INTO `sign_records` VALUES ('3', '19940790216', '1542967827', '1543120344', '152517', '1天18时21分57秒');
INSERT INTO `sign_records` VALUES ('4', '19940790216', '1543480830', '1547267756', '3786926', '43天19时55分26秒');
INSERT INTO `sign_records` VALUES ('5', '19940790216', '1553004143', null, null, null);

-- ----------------------------
-- Table structure for sign_state
-- ----------------------------
DROP TABLE IF EXISTS `sign_state`;
CREATE TABLE `sign_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sign_state
-- ----------------------------
INSERT INTO `sign_state` VALUES ('4', '19940790216', '已签到');
INSERT INTO `sign_state` VALUES ('5', '15228380046', '未签到');
INSERT INTO `sign_state` VALUES ('6', '18767167009', '未签到');
INSERT INTO `sign_state` VALUES ('7', '17353994769', '未签到');

-- ----------------------------
-- Table structure for staff_outsourcing
-- ----------------------------
DROP TABLE IF EXISTS `staff_outsourcing`;
CREATE TABLE `staff_outsourcing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `outsourcingname` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff_outsourcing
-- ----------------------------
INSERT INTO `staff_outsourcing` VALUES ('1', '19940790216', '求购设备指纹算法', '待审核');
INSERT INTO `staff_outsourcing` VALUES ('2', '19940790216', '\r\n微信小程序页面载入速度自动测试', '已接包');

-- ----------------------------
-- Table structure for staff_task
-- ----------------------------
DROP TABLE IF EXISTS `staff_task`;
CREATE TABLE `staff_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `taskId` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff_task
-- ----------------------------

-- ----------------------------
-- Table structure for taskinfo
-- ----------------------------
DROP TABLE IF EXISTS `taskinfo`;
CREATE TABLE `taskinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) NOT NULL,
  `promulgator` varchar(255) NOT NULL,
  `taskName` varchar(255) NOT NULL,
  `authority` varchar(255) NOT NULL,
  `taskContent` varchar(255) NOT NULL,
  `releaseTime` varchar(255) NOT NULL,
  `taskState` varchar(255) NOT NULL,
  `missionDeadLine` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of taskinfo
-- ----------------------------
INSERT INTO `taskinfo` VALUES ('1', '微信小程序页面载入速度自动测试', '19940790216', '开发前台页面', '中', '开发前台页面开发前台页面开发前台页面开发前台页面开发前台页面开发前台页面开发前台页面开发前台页面开发前台页面', '2018/11/22 22:07:29', '待领取', '2018/11/30');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `obey` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('6', '19940790216', '张海洋', '7b6b3cd6baabd04508debf2f69cf5622', 'gentleman', 'on');
INSERT INTO `user` VALUES ('11', '18767167009', 'Sliver', '1679a552cb4ca51f6f52a91dbf977f3c', 'gentleman', 'on');
INSERT INTO `user` VALUES ('15', '17353994769', '小飞侠', '7331f88c39a6fd46db7e0e4129a2e391', 'gentleman', 'on');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `User_id` int(11) NOT NULL,
  `Role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('4', '1');
INSERT INTO `user_role` VALUES ('4', '2');
INSERT INTO `user_role` VALUES ('5', '1');
INSERT INTO `user_role` VALUES ('6', '1');
INSERT INTO `user_role` VALUES ('6', '2');
INSERT INTO `user_role` VALUES ('7', '1');
INSERT INTO `user_role` VALUES ('11', '1');
INSERT INTO `user_role` VALUES ('15', '1');
